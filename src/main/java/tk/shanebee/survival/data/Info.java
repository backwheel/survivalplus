package tk.shanebee.survival.data;

/**
 * Info for player scoreboards
 */
public enum Info {

	HUNGER,
	THIRST,
	FATIGUE,
	NUTRIENTS

}
